PROJECT = "tempdemo"
VERSION = "1.0.0"

-- sys库是标配
_G.sys = require("sys")


-- SDA      PA4     4
-- SCL      PA1     1

--初始化i2c
if i2c.setup(0, i2c.FAST, 0x44) == 1 then
    log.info("存在 i2c0")
else
    i2c.close(0) -- 关掉
end

function get_temperature()
    local w = i2c.send(0, 0x44, string.char(0x2c, 0x06))
    sys.wait(15)
    local r = i2c.recv(0, 0x44, 6)
    local a, b, c, d, e, f, g = string.unpack("BBBBBB",r)

    t = ((((a * 256) + b) * 175) / 65535) - 45
    h = ((((d * 256) + e) * 100) / 65535)

    return t, h
end

sys.taskInit(function()
    while 1 do
        temp, hum = get_temperature()
        log.info("Data", "temp:"..temp.."deg, hum:"..hum.."%")
        sys.wait(1000)
    end
end)

sys.run()
