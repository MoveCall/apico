PROJECT = "button"
VERSION = "1.0.0"

-- sys库是标配
_G.sys = require("sys")

-- BtnA     B6  22
-- BtnB     B7  23


log.info("hello luatos")

gpio.setup(22, nil)
gpio.setup(23, nil)

sys.taskInit(function()

    while 1 do
        log.info("BtnA", gpio.get(22))
        log.info("BtnB", gpio.get(23))     
        sys.wait(200)
    end
end)

sys.run()

