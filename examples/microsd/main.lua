
-- LuaTools需要PROJECT和VERSION这两个信息
PROJECT = "micro sd"
VERSION = "1.0.1"

-- sys库是标配
_G.sys = require("sys")

sys.taskInit(function() 
    log.info(" ---  SDIO Init", sdio.init(0))
    -- log.info(" ---  SDIO sd_format", sdio.sd_format(0))
    log.info(" ---  SDIO Mount", sdio.sd_mount(0,"/sd",0))

    log.info("fsstat", fs.fsstat("/sd"))

    local file_size = fs.fsize("/sd/test.txt")
    print("/sd/test.txt file_size",file_size)

    f = io.open("/sd/test.txt", "rb")

    if f then
        local data = f:read("*a")
        log.info("fs", "data", data, data:toHex())
        c = tonumber(data)
        f:close()
    end

    while true do
        sys.wait(1000)
    end
end)

-- 主循环, 必须加
sys.run()
