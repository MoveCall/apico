PROJECT = "led5x5"
VERSION = "1.0.0"

-- sys库是标配
_G.sys = require("sys")


-- MOSI     B5  21
-- SCK      B2  18
-- ST_CP    B8  24


--  XY  X1  X2  X3  X4  X5
--  Y1  D   D   D   D   D
--  Y2  D   D   D   D   D
--  Y3  D   D   D   D   D
--  Y4  D   D   D   D   D
--  Y5  D   D   D   D   D

-- 595 --
-- Q0   X5
-- Q1   X4
-- Q2   X3
-- Q3   X2
-- Q4   X1
-- |
-- 595 --
-- Q0   Y5
-- Q1   Y4
-- Q2   Y3
-- Q3   Y2
-- Q4   Y1


log.info("hello luatos")
spi.setup(0, 20, 0, 0, 8, 10 * 1000 * 1000, spi.MSB, 1, 1)
local ST_CP = gpio.setup(24, 0, gpio.PULLUP)
gpio.setup(22, nil)
gpio.setup(23, nil)

local buff = zbuff.create(3, 0x01)
local buffv = zbuff.create(5, 0x01)
local buffh = zbuff.create(5 * 11, 0x01)

buffv = {0xef, 0xf7, 0xfb, 0xfd, 0xfe}

buffh = {0x06,0x09,0x09,0x09,0x06, -- 0
0x04,0x0C,0x04,0x04,0x0E,  --1 
0x06,0x09,0x02,0x04,0x0F,  --2 
0x06,0x09,0x02,0x09,0x06,  --3 
0x02,0x06,0x0A,0x0F,0x02,  --4
0x0F,0x08,0x0E,0x01,0x0E,  --5 
0x06,0x08,0x0E,0x09,0x06,  --6 
0x0F,0x01,0x02,0x04,0x04,  --7
0x06,0x09,0x06,0x09,0x06,  --8
0x06,0x09,0x07,0x01,0x06}  --9

-- buffh = {0x0F,0x09,0x09,0x09,0x0F}  --0
-- buffh = {0x04,0x04,0x04,0x04,0x04}  --1
-- buffh = {0x0F,0x01,0x0F,0x08,0x0F}  --2
-- buffh = {0x0F,0x01,0x0F,0x01,0x0F}  --3
-- buffh = {0x09,0x09,0x0F,0x01,0x01}  --4
-- buffh = {0x0F,0x08,0x0F,0x01,0x0F}  --5
-- buffh = {0x0F,0x08,0x0F,0x09,0x0F}  --6
-- buffh = {0x0F,0x01,0x01,0x01,0x01}  --7
-- buffh = {0x0F,0x09,0x0F,0x09,0x0F}  --8
-- buffh = {0x0F,0x09,0x0F,0x01,0x0F}  --9


function show5x5LED(num)

    for j=0, 25 do
        for i=1, 5 do
            -- log.info("For", i)
            buff[0] = buffv[i]
            buff[1] = buffh[i + num * 5]
            for z=0, 20 do
                spi.send(0, buff, 2)
                ST_CP(1)
                ST_CP(0)
            end
        end
    end

end


sys.taskInit(function()
    local count = 0

    while 1 do
        -- log.info("K1", gpio.get(22))
        -- log.info("K2", gpio.get(23))
        log.info("count", count)
        if gpio.get(22) == 0 then 
            count = count + 1
        end
        if gpio.get(23) == 0 then 
            count = count - 1
        end

        if count < 0  then 
            count = 0
        end
        if count > 9 then 
            count = 9
        end
        
        show5x5LED(count)     
        -- sys.wait(10)
    end
end)

sys.run()

