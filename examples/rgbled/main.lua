

-- LuaTools需要PROJECT和VERSION这两个信息
PROJECT = "rgbled"
VERSION = "1.0.0"

-- R    PB0 16
-- G    PB1 17
-- B    PA7 7

-- sys库是标配
_G.sys = require("sys")

local LED_R = gpio.setup(16, 0, gpio.PULLUP) 
local LED_G = gpio.setup(17, 0, gpio.PULLUP) 
local LED_B = gpio.setup(7, 0, gpio.PULLUP) 

LED_R(1)
LED_G(1)
LED_B(1)

log.info("hello luatos")

sys.taskInit(function()

    while 1 do
        LED_R(0)
        LED_G(1)
        LED_B(1)
        sys.wait(300)

        LED_R(1)
        LED_G(0)
        LED_B(1)
        sys.wait(300)
        
        LED_R(1)
        LED_G(1)
        LED_B(0)
        sys.wait(300)
    end
    
end)


-- 用户代码已结束---------------------------------------------
-- 结尾总是这一句
sys.run()
-- sys.run()之后后面不要加任何语句!!!!!


