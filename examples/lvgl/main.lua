-- LuaTools需要PROJECT和VERSION这两个信息
PROJECT = "lvgl"
VERSION = "1.0.0"

-- MOSI B5  21
-- CD   B3  19
-- SCK  B2  18
-- CS   B4  20
-- RST  B9  25

-- K1   B6  22
-- K2   B7  23

-- sys库是标配
_G.sys = require("sys")

--初始化i2c，air101使用id为1
if i2c.setup(0, i2c.FAST, 0x44) == 1 then
    log.info("存在 i2c0")
else
    i2c.close(0) -- 关掉
end

log.info("hello luatos")
spi.setup(0, 20, 0, 0, 8, 40 * 1000 * 1000, spi.MSB, 1, 1)
log.info("lcd.init", lcd.init("st7789",{port = 0,pin_cs = 20,pin_dc = 19, pin_pwr = 0,pin_rst = 25,direction = 2,w = 240,h = 135,xoffset = 40,yoffset = 53}))

log.info("lvgl", lvgl.init())
lvgl.disp_set_bg_color(nil, 0xFFFFFF)
local scr = lvgl.obj_create(nil, nil)


-- # Line Chart #
local chart_demo = {}
local DataArray = {10, 0, 0, 0, 0, 0, 0, 0, 0, 0}
local DataHunArray = {10, 0, 0, 0, 0, 0, 0, 0, 0, 0}
local chart;

function chart_demo.demo2()
    --Create a chart
    -- local chart;
    chart = lvgl.chart_create(scr);
    lvgl.obj_set_size(chart, 200, 100);
    lvgl.obj_align(chart, nil, lvgl.ALIGN_CENTER, 0, 0);
    lvgl.chart_set_type(chart, lvgl.CHART_TYPE_LINE);   --Show lines and points too

    --Add a faded are effect
    lvgl.obj_set_style_local_bg_opa(chart, lvgl.CHART_PART_SERIES, lvgl.STATE_DEFAULT, lvgl.OPA_50); --Max. opa.
    lvgl.obj_set_style_local_bg_grad_dir(chart, lvgl.CHART_PART_SERIES, lvgl.STATE_DEFAULT, lvgl.GRAD_DIR_VER);
    lvgl.obj_set_style_local_bg_main_stop(chart, lvgl.CHART_PART_SERIES, lvgl.STATE_DEFAULT, 255);    --Max opa on the top
    lvgl.obj_set_style_local_bg_grad_stop(chart, lvgl.CHART_PART_SERIES, lvgl.STATE_DEFAULT, 0);      --Transparent on the bottom

    --Add two data series
    local ser1 = lvgl.chart_add_series(chart, lvgl.color_make(0xFF, 0x00, 0x00));
    local ser2 = lvgl.chart_add_series(chart, lvgl.color_make(0x00, 0x80, 0x00));
    --Set the next points on 'ser1'
    lvgl.chart_set_next(chart, ser1, DataArray[0]);
    lvgl.chart_set_next(chart, ser1, DataArray[1]);
    lvgl.chart_set_next(chart, ser1, DataArray[2]);
    lvgl.chart_set_next(chart, ser1, DataArray[3]);
    lvgl.chart_set_next(chart, ser1, DataArray[4]);
    lvgl.chart_set_next(chart, ser1, DataArray[5]);
    lvgl.chart_set_next(chart, ser1, DataArray[6]);
    lvgl.chart_set_next(chart, ser1, DataArray[7]);
    lvgl.chart_set_next(chart, ser1, DataArray[8]);
    lvgl.chart_set_next(chart, ser1, DataArray[9]);

    --Directly set points on 'ser2'
    lvgl.chart_set_next(chart, ser2, DataHunArray[0]);
    lvgl.chart_set_next(chart, ser2, DataHunArray[1]);
    lvgl.chart_set_next(chart, ser2, DataHunArray[2]);
    lvgl.chart_set_next(chart, ser2, DataHunArray[3]);
    lvgl.chart_set_next(chart, ser2, DataHunArray[4]);
    lvgl.chart_set_next(chart, ser2, DataHunArray[5]);
    lvgl.chart_set_next(chart, ser2, DataHunArray[6]);
    lvgl.chart_set_next(chart, ser2, DataHunArray[7]);
    lvgl.chart_set_next(chart, ser2, DataHunArray[8]);
    lvgl.chart_set_next(chart, ser2, DataHunArray[9]);

    lvgl.chart_refresh(chart); --Required after direct set
end
-- chart_demo.demo1()


local label2 = lvgl.label_create(scr);
lvgl.label_set_long_mode(label2, lvgl.LABEL_LONG_SROLL_CIRC);     --Circular scroll
lvgl.obj_set_width(label2, 240);
lvgl.label_set_text(label2, "Temperature 25 deg, relative humidity 66%");
lvgl.obj_align(label2, nil, lvgl.ALIGN_CENTER, 0, -60);

function label_demo(t, h)
    lvgl.label_set_text(label2, "Temperature " .. t .. " deg, relative humidity " .. h .. "%");
end


function get_temperature()
    local w = i2c.send(0, 0x44, string.char(0x2c, 0x06))
    sys.wait(15)
    local r = i2c.recv(0, 0x44, 6)
    local a, b, c, d, e, f, g = string.unpack("BBBBBB",r)

    t = ((((a * 256) + b) * 175) / 65535) - 45
    h = ((((d * 256) + e) * 100) / 65535)

    return t, h
end

DataArray[0] = 10
DataHunArray[0] = 10
chart_demo.demo2()

-- lvgl.scr_load(scr)

sys.taskInit(function()
    local count, f5 = 0, 0
    local temp, hum, tempnum = 0, 0, 0
    while 1 do

        count = (count + 1) % 10
        if count == 0 then
            tempnum = (tempnum + 1) % 10
            temp, hum = get_temperature()    
            DataArray[tempnum] = temp
            DataHunArray[tempnum] = hum 
    
            chart_demo.demo2()
    
            log.info("temp", temp)
            log.info("hum", hum)

            if f5 == 0 then
                label_demo(DataArray[tempnum], DataHunArray[tempnum])
            end
            f5 = (f5 + 1) % 100

        end

        lvgl.scr_load(scr)
        sys.wait(100)
    end
end)


-- 用户代码已结束---------------------------------------------
-- 结尾总是这一句
sys.run()
-- sys.run()之后后面不要加任何语句!!!!!


